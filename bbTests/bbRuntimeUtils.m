//
//  bbRuntimeUtils.m
//  bbTests
//
//  Created by Roberto Previdi on 25/06/13.
//  Copyright (c) 2013 bentobox. All rights reserved.
//

#import "bbRuntimeUtils.h"
#import <objc/runtime.h>

@implementation bbRuntimeUtils
+(void)getInstancesOfSubclassesOf:(Class)parentClass putThemInto:(NSMutableSet*)result
{
	[bbRuntimeUtils getSubclassesOf:parentClass instantiated:true putThemInto:result];
}
+(void)getSubclassesOf:(Class)parentClass putThemInto:(NSMutableSet *)result
{
	[bbRuntimeUtils getSubclassesOf:parentClass instantiated:false putThemInto:result];
}
+(void)getSubclassesOf:(Class)parentClass instantiated:(BOOL)instantiated putThemInto:(NSMutableSet *)result
{
	int numClasses = objc_getClassList(NULL, 0);
    Class *classes = NULL;
	
    classes = (__unsafe_unretained Class*)malloc(sizeof(Class) * numClasses);
    numClasses = objc_getClassList(classes, numClasses);
	
    for (NSInteger i = 0; i < numClasses; i++)
    {
        Class superClass = classes[i];
        do
        {
            superClass = class_getSuperclass(superClass);
        } while(superClass && superClass != parentClass);
		
        if (superClass == nil)
        {
            continue;
        }
		Class c=NSClassFromString([NSString stringWithCString: class_getName(classes[i]) encoding:NSUTF8StringEncoding ]);
		
		if (instantiated)
		{
			id obj=[[c alloc]init];
			[result addObject:obj];
		}
		else
			[result addObject:c];
    }
	
    free(classes);
}


@end
