//
//  bbTestXCTestCompatibility.h
//  bbTests
//
//  Created by roberto previdi on 11/10/13.
//  Copyright (c) 2013 bentobox. All rights reserved.
//
/* This file is useful if you want to cut and paste your bbTest
   test cases into the new XCTest library. it translates the
   assertions. 
   This must be included after XCTest.h
   This must NOT be included together with bbTestCase.h */

#ifndef bbTests_bbTestXCTestCompatibility_h
#define bbTests_bbTestXCTestCompatibility_h

#define assertEquals( x , y ) XCTAssertEqual(x,y,@"assertEquals failed")
#define assertEqualsInt( x , y ) XCTAssertEqual((NSInteger)x,(NSInteger)y,@"assertEqualsInt failed: Actually "#x"=%d and "#y"=%d",(int)x,(int)y)
#define assertEqualsString( x , y ) XCTAssertEqualObjects(x,y,@"assertEqualsString failed")
#define assertDiffers( x , y ) XCTAssertNotEqual((NSInteger)x,(NSInteger)y,@"assertDiffers failed")
#define assertTrue( x ) XCTAssertTrue(x,@"assertTrue failed")
#define assertFalse( x ) XCTAssertFalse(x,@"assertFalse failed")
#define assertNull( x ) XCTAssertNil(x,@"assertNull failed")
#define assertNotNull( x ) XCTAssertNotNil(x,@"assertNotNull failed")
#define assertThrowsStart @try{
#define assertThrowsEnd XCTAssertTrue(false,@"The block was expected to throw an exception, but it didn't");}@catch(id x){XCTAssertTrue(true,@"no exception thrown");}
//#define assertRet( x , y ) assertEqualsInt( [x retainCount] , y )
//ARC switch

#endif
