//
//  bbLog.m
//  bbTests
//
/*
 Copyright 2013 Roberto Previdi <hariseldon78@gmail.com>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "bbLog.h"

@interface _bbLog ()
+(NSString*)indent;
+(void)doOutput:(NSString*)formattedString;
@end

#define MAX_ROW_LENGTH 100
// attenzione, con i buffer attivi si perdono gli ultimi log prima della chiusura del programma. disattivare durante il debugging
//#define USE_BUFFERS 1
//#define ON_MAIN_THREAD 1
void _customLog(BOOL buffered,NSString* str)
{
	NSMutableString* formattedString=[[NSMutableString alloc]initWithCapacity:str.length+20 ];
	NSString* ind=[_bbLog indent];
	//	NSInteger availableRowSpace=MAX_ROW_LENGTH-ind.length;
	NSArray* rows=[[str stringByReplacingOccurrencesOfString:@"\t" withString:@"    "] // si può fare molto meglio, ma per ora ci accontentiamo
				   componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n\r"]];
	//	NSString* prefix=@"";
	for (int j=0;j<rows.count;j++)
	{
		NSString* s=rows[j];
		for (int i=0;s.length>0;i++)
		{
			[formattedString appendString:ind];
			
			int space_available=MAX(MAX_ROW_LENGTH-ind.length-(i>0?5:0),10);
			NSString* s_part;
			if (s.length>space_available)
			{
				if (i>0) [formattedString appendString:@"     "];
				s_part=[s substringToIndex:space_available];
				s=[s substringFromIndex:space_available];
			}
			else
			{
				if (i>0) [formattedString appendString:@"     "];
				s_part=s;
				s=@"";
			}
			
			[formattedString appendString:s_part];
			/*if (s.length) */[formattedString appendString:@"\n"];
		}
	}
	//elimino l'ultimo ritorno a capo
	if (formattedString.length>0 && [formattedString characterAtIndex:formattedString.length-1]=='\n')
		[formattedString deleteCharactersInRange:NSMakeRange(formattedString.length-1, 1)];
	
	
	if (buffered)
	{
		NSMutableArray* a=([NSThread currentThread].threadDictionary)[@"logBuffer"];
		if (!a)
		{
			a=[[NSMutableArray alloc]initWithCapacity:10];
			([NSThread currentThread].threadDictionary)[@"logBuffer"] = a;
		}
		[a addObject:formattedString];
	}
	else
		[_bbLog doOutput:formattedString];
}
void customLogBuffered(NSString* s,...)
{
	va_list args;
	va_start(args, s);
	s=[[NSString alloc ]initWithFormat:s arguments:args];
	va_end(args);
	_customLog(true, s);
}
void customLogUnbuffered(NSString* s,...)
{
	va_list args;
	va_start(args, s);
	s=[[NSString alloc ]initWithFormat:s arguments:args];
	va_end(args);
	_customLog(false, s);
}

#ifdef USE_BUFFERS
#define customLog customLogBuffered

void consumeBuffer()
{
	NSMutableArray* a=[[NSThread currentThread].threadDictionary objectForKey:@"logBuffer"];
	if (a)
	{
		for (NSString* s in a)
			doOutput(s);
		[a removeAllObjects];
	}
}

#else

#define customLog customLogUnbuffered

#endif


static unsigned int indentationLevel=0;
static NSMutableArray* accumulator=nil;
@implementation _bbLog
- (id)init
{
    self = [super init];
    if (self) {
        indentationLevel++;
    }
    return self;
}
-(void)dealloc
{
#ifdef USE_BUFFERS
	consumeBuffer();
#endif
	indentationLevel--;
}
+(NSString*)indent
{
	int indLev=indentationLevel;
	if (indLev<0) indLev=0;
	if (indLev>9) indLev=9;
	
	NSString* s=[@"" stringByPaddingToLength:indLev*4 withString:@"    " startingAtIndex:0];
#ifdef DEBUG_THREADS
	NSString* threadName=[[globals threadName] stringByPaddingToLength:4 withString:@" " startingAtIndex:0];
	s=[NSString stringWithFormat:@"%@  %@",threadName,s];
#endif
	return s;
}
+(void)log:(NSString*)s
{
/*	NSMutableString* ms=[NSMutableString stringWithString:s];
	NSMutableString* indentation=[NSMutableString stringWithString:[@"" stringByPaddingToLength:indentationLevel withString:@"\t" startingAtIndex:0]];
	[ms insertString:indentation atIndex:0];
	[indentation appendString:@"  "];
	[ms replaceOccurrencesOfString:@"\n" withString:indentation options:0 range:NSMakeRange(0, ms.length)];
	
	printf("%s\n",[ms UTF8String]);*/
#ifdef ON_MAIN_THREAD 
	if ([NSThread isMainThread])
		customLog(s);
	else
		[[NSOperationQueue mainQueue]addOperationWithBlock:^{
			customLog(s);
		}];
#else
	customLog(s);
#endif
}
+(void)setAccumulator:(NSMutableArray*)_accumulator
{
	accumulator=_accumulator;
}
+(void)consumeAccumulator
{
	NSMutableArray* tmp=accumulator;
	accumulator=nil;
	for (NSString* s in tmp)
		[_bbLog doOutput:s];
}
+(void)trashAccumulator
{
	accumulator=nil;
}
+(void)doOutput:(NSString*)formattedString
{
	if (accumulator)
	{
		[accumulator addObject:formattedString];
		return;
	}
#ifdef USE_NSLOG
	NSLogv("%@",formattedString);
#else
	printf("%s\n",[formattedString UTF8String]);
#endif
}

+(NSString*)color:(UIColor*)c
{
	CGFloat red,green,blue,alpha;
	[c getRed:&red green:&green blue:&blue alpha:&alpha];
	return [NSString stringWithFormat:@"[%d,%d,%d,%d]",(int)(red*255),(int)(green*255),(int)(blue*255),(int)(alpha*255)];
	
//	const CGFloat* components=CGColorGetComponents(c.CGColor);
//	size_t componentCount=CGColorGetNumberOfComponents(c.CGColor);
//	NSMutableString* ms=[[NSMutableString alloc]init];
//	[ms appendFormat:@"["];
//	for (int i=0;i<componentCount;i++)
//	{
//		[ms appendFormat:@"%d,",(int)(255*components[i])];
//	}
//	[ms appendFormat:@"]"];
//	return ms;
}


@end
