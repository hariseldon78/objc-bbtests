//
//  MyTestCase.h
//  NavigationPdfViewer
//
/*
 Copyright 2013 Roberto Previdi <hariseldon78@gmail.com>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#import <UIKit/UIKit.h>
#define ME(x) x

#define QUOTEME_(x) #x
#define QUOTEME(x) QUOTEME_(x)
#define WHERE QUOTEME( __FILE__ ) ":" QUOTEME( __LINE__ ) ": "
#define assertEquals( x , y ) { if ((x)==(y)) {[self assertionOk];} else {[self assertionFailedWithMessage: @WHERE"("#x"=="#y")"];};}
#define assertEqualsInt( x , y ) { if ((x)==(y)) {[self assertionOk];} else {[self assertionFailedWithMessage: [NSString stringWithFormat: @WHERE"("#x"=="#y"). Actually "#x"=%d and "#y"=%d",x,y]];};}
#define assertEqualsString( x , y ) { if ([x isEqualToString:y]) {[self assertionOk];} else {[self assertionFailedWithMessage: [NSString stringWithFormat: @WHERE"["#x" isEqualToString "#y"]. Actually "#x"=\"%@\" and "#y"=\"%@\"",x,y]];};}

#define assertDiffers( x , y ) { if ((x)!=(y)) {[self assertionOk];} else {[self assertionFailedWithMessage: @WHERE"("#x"!="#y") is FALSE"];};}
#define assertTrue( x ) { if (x) {[self assertionOk];} else {[self assertionFailedWithMessage: @WHERE"("#x") is FALSE"];};}
#define assertFalse( x ) assertTrue( ! x )
#define assertNull( x ) assertEquals( x , NULL )
#define assertNotNull( x ) assertDiffers( x , NULL )
#define assertThrowsStart @try{
#define assertThrowsEnd [self assertionFailedWithMessage:@"The block was expected to throw an exception, but it didn't"];}@catch(id x){[self assertionOk];}
//#define assertRet( x , y ) assertEqualsInt( [x retainCount] , y )
//ARC switch
#define assertRet( x , y ) assertEqualsInt( 1 , 1 )

@class bbTests;

@interface bbTestCase : NSObject {
	bbTests* counter;
}

-(NSArray*)configurations;
-(void)assertionOk;
-(void)assertionFailedWithMessage:(NSString*)msg;
-(void)forceLogging;
//-(void)run;

@property (nonatomic,strong) bbTests* counter;
@property (nonatomic,strong) UITextView* textView;
@property (strong) NSString* currentConfiguration;
@end
