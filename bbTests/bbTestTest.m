//
//  bbTestTest.m
//  bbTests
//
/*
 Copyright 2013 Roberto Previdi <hariseldon78@gmail.com>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#import "bbTestTest.h"
@interface bbTestTest ()
{
	int initializedAtBoot;
}

@end

@implementation bbTestTest

-(void)tearUp
{
	initializedAtBoot=5;
}

-(void)testOneIsOne
{
	assertEqualsInt(1, 2-1);
}

-(void)testTearUpIsRun
{
	assertEqualsInt(5, initializedAtBoot);
	initializedAtBoot++;
}

-(void)testTearUpIsRunAgain
{
	assertEqualsInt(5, initializedAtBoot);
	initializedAtBoot++;
}


@end
