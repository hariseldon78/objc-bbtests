//
//  bbTests.m
//  bbTests
//
/*
 Copyright 2013 Roberto Previdi <hariseldon78@gmail.com>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#import "bbTests.h"
#import "bbTestCase.h"
#import <UIKit/UIKit.h>
#import <MacTypes.h>
#import "bbLog.h"
#import "bbRuntimeUtils.h"
#import <objc/runtime.h>

@interface bbTests ()
{
	int failedMethodsCount;
	int doneMethodsCount;
	UIProgressView* successProgress;
	UIProgressView* totalProgress;
}
@property (assign) int totalMethodsCount;
@end

@implementation bbTests

@synthesize totalAsserts;
@synthesize failedAsserts;

-(id)initWithTotalProgress:(UIProgressView*)_totalProgress successProgress:(UIProgressView*)_successProgress
{
	self=[super init];
	failedMethodsCount=0;
	doneMethodsCount=0;
	totalAsserts=0;
	failedAsserts=0;
	totalProgress=_totalProgress;
	successProgress=_successProgress;
	
	return self;
}


+(BOOL)runAllOnTextView:(UITextView*)textView totalProgress:(UIProgressView*)totalProgress successProgress:(UIProgressView*)successProgress
{
	bbLogF;
	
	NSMutableSet* testCases=[[NSMutableSet alloc]initWithCapacity:10];
	[bbRuntimeUtils getInstancesOfSubclassesOf:bbTestCase.class putThemInto:testCases];
	return [bbTests runWithSet:testCases onTextView:textView totalProgress:totalProgress successProgress:successProgress];
}

+(BOOL)runWithSet:(NSSet*)testCases onTextView:(UITextView*)textView totalProgress:(UIProgressView*)totalProgress successProgress:(UIProgressView*)successProgress
{
	bbLogF;
	bbLog(@"Starting tests");
	
	
	
	
	bbTests* me=[[bbTests alloc]initWithTotalProgress:totalProgress successProgress:successProgress];
	__block unsigned int _totalMethodCount=0;
	NSMutableDictionary* allTestMethods=[[NSMutableDictionary alloc]initWithCapacity:testCases.count];
	[testCases enumerateObjectsUsingBlock:^(id obj, BOOL *stop)
	 {
		 NSMutableArray* methodsNames=[[NSMutableArray alloc]initWithCapacity:10];
		 if ([obj respondsToSelector:@selector(bbTestSubSet)])
		 {
			 NSArray* subSet=[obj performSelector:@selector(bbTestSubSet)];
			 for (NSString* s in subSet)
			 {
				 SEL sel=NSSelectorFromString(s);
				 if ([obj respondsToSelector:sel])
				 {
					 [methodsNames addObject:s];
				 }
			 }
		 }
		 else
		 {
			 unsigned int methodCount;
			 Method *methods=class_copyMethodList(object_getClass(obj), &methodCount);
			 for (int i=0;i<methodCount;i++)
			 {
				 SEL s=method_getName(methods[i]);
				 const char* name=sel_getName(s);
				 NSString* nameString=@(name);
				 if ([nameString commonPrefixWithString:@"test" options:NSCaseInsensitiveSearch].length==4)
					 [methodsNames addObject:nameString];
			 }
			 
			 free(methods);
		 }
		 NSArray* configurations=((bbTestCase*)obj).configurations;
		 _totalMethodCount+=methodsNames.count*configurations.count;
		 allTestMethods[@(object_getClassName(obj))] = methodsNames;
	 }];
	me.totalMethodsCount=_totalMethodCount;
	
	if (totalProgress)
		[[NSOperationQueue mainQueue]addOperation:[NSBlockOperation blockOperationWithBlock:^{
			[totalProgress setProgress:0.0];
			[successProgress setProgress:0.0];
			[successProgress setProgressTintColor:[UIColor greenColor] ];
			[successProgress setTrackTintColor:[UIColor redColor] ];
		}]];
	
	
	[testCases enumerateObjectsUsingBlock:^(id obj, BOOL *stop)
	 {
		 [self appendText:[NSString stringWithFormat:@"running test suite: %s",object_getClassName(obj)]
			   toTextView:textView];
		 [obj setCounter:me];
		 [obj setTextView:textView];
		 NSArray* methodsNames=allTestMethods[@(object_getClassName(obj))];
		 
/*		 unsigned int methodCount=((NSNumber*)[methodsDictionary objectForKey:@"count"]).integerValue;
		 Method *testMethods=((NSValue*)[methodsDictionary objectForKey:@"methods"]).pointerValue;*/
		 
		 
		 [me runTestCase:methodsNames.count
				  textView:textView
					   obj:obj
				   methods:methodsNames];
		 
		 //					   [obj run];
	 }];
	if (totalProgress)
		[self appendText:[NSString stringWithFormat:@"\nTests finished: %d total, %d failed",me.totalAsserts, me.failedAsserts] toTextView:textView];
	BOOL ok=me.failedAsserts==0;
	return ok;
}

- (void)runTestCase:(unsigned int)methodCount
		   textView:(UITextView *)textView
				obj:(bbTestCase*)obj
			methods:(NSArray*)methods
{
    int failedTests=0;
	NSArray* configurations=[obj configurations];
	if (!configurations || configurations.count==0)
		configurations=@[@""];
	for (NSString* currentConfiguration in configurations)
	{
		obj.currentConfiguration=currentConfiguration;
		if (! [currentConfiguration isEqualToString:@""])
		{
			if (totalProgress)
				[bbTests appendText:[NSString stringWithFormat:@"selecting configuration: %@",currentConfiguration] toTextView:textView];
			
		}
		for (int i=0;i<methods.count;i++)
		{
			doneMethodsCount++;
			//			 [self appendText:@"\n" toTextView:textView];
			NSString* methodName=methods[i];
			SEL sel=NSSelectorFromString(methodName);
			//		Method m=class_getClassMethod(object_getClass(obj), sel);
			int oldtotal=totalAsserts;
			int oldfailed=failedAsserts;
			_bbLog* __bbLog_obj;
			@try {
				NSMutableArray* ma=[[NSMutableArray alloc]initWithCapacity:10];
				[_bbLog setAccumulator:ma];
				for (int i=0;i<6;i++)
					bbLog(@"");

				
				bbLog(@"___ starting test %@ ___",NSStringFromSelector(sel));
				{
					__bbLog_obj=[[_bbLog alloc]init];
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
					if ([obj respondsToSelector:@selector(tearUp)])
					{
						bbLog(@"___ runnung tearUp ___");
						[obj performSelector:@selector(tearUp)];
						bbLog(@"----------------------");
						bbLog(@"");
						bbLog(@"");
					}
					NSString* configTearupString=[NSString stringWithFormat:@"tearUp_%@",currentConfiguration];
					SEL configTearup=NSSelectorFromString(configTearupString);
					if ([obj respondsToSelector:configTearup])
					{
						bbLog(@"___ runnung %@ ___",configTearupString);
						[obj performSelector:configTearup];
						bbLog(@"----------------------");
						bbLog(@"");
						bbLog(@"");
					}
					[obj performSelector:sel];
					NSString* configTeardownString=[NSString stringWithFormat:@"tearDown_%@",currentConfiguration];
					SEL configTeardown=NSSelectorFromString(configTeardownString);
					if ([obj respondsToSelector:configTeardown])
					{
						bbLog(@"___ runnung %@ ___",configTeardownString);
						[obj performSelector:configTeardown];
						bbLog(@"----------------------");
						bbLog(@"");
						bbLog(@"");
					}
					if ([obj respondsToSelector:@selector(tearDown)])
					{
						bbLog(@"");
						bbLog(@"");
						bbLog(@"___ runnung tearDown ___");
						[obj performSelector:@selector(tearDown)];
						bbLog(@"----------------------");
					}
	#pragma clang diagnostic pop
				}
				__bbLog_obj=nil;
		   }
			@catch (NSException *e) {
				__bbLog_obj=nil;
				[bbTests appendText:[NSString stringWithFormat:@"Exception: %@",[e name]] toTextView:textView];
				totalAsserts++;
				failedAsserts++;
			}
			int tmpFailedAsserts=failedAsserts-oldfailed;
			NSString* report=nil;
			if (tmpFailedAsserts>0)
			{
				bbLog(@"^X^^^^^^^^^^^^^^XXX^^^^^^^^^^^^^^^^^^^^^^X^");
				[_bbLog consumeAccumulator];
				report=[NSString stringWithFormat:@"- %@ ok:%d KO:%d",
						[methodName
						 stringByPaddingToLength:30
						 withString:@" "
						 startingAtIndex:0],
						totalAsserts-oldtotal-tmpFailedAsserts,
						tmpFailedAsserts];
			}
			else{
				[_bbLog trashAccumulator];
				report=[NSString stringWithFormat:@"- %@ ok:%d",
						[methodName
						 stringByPaddingToLength:30
						 withString:@" "
						 startingAtIndex:0],
						totalAsserts-oldtotal];
			}
			[bbTests appendText:report toTextView:textView];
			if (tmpFailedAsserts!=0)
			{
				failedTests++;
				failedMethodsCount++;
			}
			if (totalProgress)
				[self performSelectorOnMainThread:@selector(updateBars) withObject:nil waitUntilDone:YES];
		}
	}
}

-(void)updateBars
{
	CGFloat successValue=(0.0f+doneMethodsCount-failedMethodsCount)/self.totalMethodsCount;
	if (successValue<1.0 )
		successValue=MIN(0.8,successValue);
	[totalProgress setProgress:(0.0f+doneMethodsCount)/self.totalMethodsCount];
	[successProgress setProgress:successValue];
}

+(void)appendText:(NSString*)s toTextView:(UITextView*)textView
{
	//	bbLogF;
	bbLog(@"%@",s);
	[[NSOperationQueue mainQueue]addOperation:[NSBlockOperation blockOperationWithBlock:^{
		[textView setText:[NSString stringWithFormat:@"%@\n%@",[textView text],s]];
		[textView scrollRangeToVisible:NSMakeRange(textView.text.length-2, 1)];
	}]];
}

@end
