//
//  MyTestCase.m
//  NavigationPdfViewer
//
/*
 Copyright 2013 Roberto Previdi <hariseldon78@gmail.com>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
#import "bbTests.h"
#import "bbLog.h"
#import "bbTestCase.h"

@implementation bbTestCase

@synthesize counter;
@synthesize textView;

-(id)init
{
	self=[super init];
	return self;
}
-(void)assertionOk {
	
	counter.totalAsserts++;
}

-(NSArray*)configurations
{
	return @[@""];
}
-(void)assertionFailedWithMessage:(NSString*)msg {
	counter.totalAsserts++;
	counter.failedAsserts++;
	[bbTests appendText:[NSString stringWithFormat:@"Assertion failed: %@", msg ] toTextView:self.textView];
	
}
-(void)forceLogging
{
	[_bbLog trashAccumulator];
}
@end
