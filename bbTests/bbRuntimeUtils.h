//
//  bbRuntimeUtils.h
//  bbTests
//
//  Created by Roberto Previdi on 25/06/13.
//  Copyright (c) 2013 bentobox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface bbRuntimeUtils : NSObject

+(void)getInstancesOfSubclassesOf:(Class)parentClass putThemInto:(NSMutableSet*)result;
+(void)getSubclassesOf:(Class)parentClass putThemInto:(NSMutableSet *)result;

@end
