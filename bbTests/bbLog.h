//
//  bbLog.h
//  bbTests
//
/*
 Copyright 2013 Roberto Previdi <hariseldon78@gmail.com>
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import <UIKit/UIKit.h>

#ifdef DEBUG

#define bbLog(args... ) [_bbLog log: [NSString stringWithFormat: args ] ]
#define bbLogF	bbLog(@"%@",[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding]);\
				_bbLog* __bbLog_obj=[[_bbLog alloc]init];[__bbLog_obj class]
#define bbLogVarf(x,format) bbLog(@"%@: "format,@ #x,x)
#define bbLogVar(x) bbLogVarf(x,"%@");
#define bbLogInt(x) bbLogVarf(x,"%d")
#define bbLogDouble(x) bbLogVarf(x,"%f")
#define bbLogBool(x) bbLog(@"%@: %@",@ #x,x?@"YES":@"NO")
#define bbLogPointer(x) bbLogVarf(x,"%p");
#define bbLogColor(x) bbLogVar([_bbLog color:x]);
#else

#define bbLog(args... )
#define bbLogF
#define bbLogVarf(x,y)
#define bbLogInt(x)
#define bbLogDouble(x)
#define bbLogVar(x)
#define bbLogBool(x)
#define bbLogPointer(x)
#define bbLogColor(x)
#endif


@interface _bbLog : NSObject
+(void)log:(NSString*)s;
+(void)setAccumulator:(NSMutableArray*)_accumulator;
+(void)consumeAccumulator;
+(void)trashAccumulator;

+(NSString*)color:(UIColor*)c;
@end
